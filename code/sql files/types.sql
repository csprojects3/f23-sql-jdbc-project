--types
create or replace type province_type as object(
    province_id NUMBER(2),
    province    VARCHAR2(20),
    country     VARCHAR2(25)
);
/

create or replace type location_type as object(
    city        VARCHAR2(25),
    province_id number(2)
);
/

create or replace type customers_type as object(
    customer_id NUMBER(2),
    fname       VARCHAR2(30),
    lname       VARCHAR2(30),
    email       VARCHAR2(50)
);
/

create or replace type ADDRESS_type as object(
    CUSTOMER_ID number(2),
    address VARCHAR2(50),
    city VARCHAR2(25)
);
/

create or replace type products_type as object(
    product_id  NUMBER(2),
    name        varchar2(30),
    category    VARCHAR2(30),
    price       NUMBER(10,2)
);
/

create or replace TYPE stores_type as OBJECT(
    store_id NUMBER(2),
    name     VARCHAR2(30)
);
/

create or replace TYPE reviews_type as OBJECT(
    review_id   NUMBER(2),
    score       NUMBER(2),
    flag        NUMBER(1),
    DESCRIPTION VARCHAR2(600)
);
/

create or replace TYPE order_info_type as OBJECT(
    order_id    NUMBER(2),
    order_date  DATE,
    quantity    NUMBER(3)
);
/

create or replace TYPE WAREHOUSE_INFO_type as OBJECT(
    name                VARCHAR2(30),
    warehouse_address   VARCHAR2(30),
    city                VARCHAR2(25)
);
/

create or replace TYPE WAREHOUSE_type as OBJECT(
    name        VARCHAR2(30),
    product_id  number(4),
    quantity    number(7)
);
/

create or replace TYPE orders_type as OBJECT(
    order_id    NUMBER(2),
    customer_id NUMBER(2),
    product_id  NUMBER(2),
    store_id    NUMBER(2),
    review_id   NUMBER(2)
);
/

