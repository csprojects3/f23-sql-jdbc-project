/**
 * a class for validating data
 */
package utilities;

public class Validator {

    public void validateInt(int numToValidate,int min,int max){
        if(numToValidate <= min && numToValidate > max){ 
            throw new IllegalArgumentException("Number out of range " + min + " to " + max);
        }
    }
    public void validateString(String validateString,int maxLength){
        if(validateString.length() > maxLength){
            throw new IllegalArgumentException(validateString + " greater than max length: " + maxLength);
        }
    }
    public void validateDouble(Double validateDouble, double min,double max){
        if(validateDouble < min || validateDouble >= max ){
            throw new IllegalArgumentException("Invalid double value. Must be in range: " + min + " to: " + max);
        }
        String[] divide = validateDouble.toString().split("\\.");
        if(divide[1].length() > 2){
            throw new IllegalArgumentException("Invalid double value: " + validateDouble + ". Max of 2 decimal digits");
        }
    }
}
