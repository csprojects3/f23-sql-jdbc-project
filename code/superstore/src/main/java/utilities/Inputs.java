/**
 * a class for getting inputs using a scanner
 */
package utilities;
import java.util.Scanner;

public class Inputs {
    private Scanner in;
    public Inputs(){
        this.in = new Scanner(System.in);
    }

    public int getUserInteger(){
        return Integer.parseInt(this.in.nextLine());
    }

    public String getUserString(){
        return this.in.nextLine();
    }

    public double getUserDouble(){
        return Double.parseDouble(this.in.nextLine());
    } 

    public void close(){
        this.in.close();
    }
}
