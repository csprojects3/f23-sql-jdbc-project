package tables;

import utilities.Validator;

public class Warehouse{

    public final String type_name = "WAREHOUSE_TYPE";
    private String name;
    private int product_id;
    private int quantity;

    
    public Warehouse(String name, int product_id, int quantity) {
        Validator check = new Validator();
        check.validateString(name, 30);
        check.validateInt(product_id, 0, 99);
        check.validateInt(quantity, 0, 9999999);

        this.name = name;
        this.product_id = product_id;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Warehouse [name=" + name + ", product_id=" + product_id + ", quantity=" + quantity + "]";
    }
}
