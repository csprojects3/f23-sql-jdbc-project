package tables;

import java.sql.*;
import java.util.Map;

import utilities.Validator;

public class Products implements SQLData{
    
    public final  String type_name = "PRODUCTS_TYPE";
    private int productID;
    private String name;
    private String category;
    private double price;
    
    public Products(int productID, String name, String category, double price) {
        Validator check = new Validator();
        check.validateInt(productID, 0, 99);
        check.validateString(name, 30);
        check.validateString(category, 30);
        check.validateDouble(price, 0, 99999999);

        this.productID = productID;
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public Products(){}
    
    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.type_name;
    }
    
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductID(stream.readInt());
        setName(stream.readString());
        setCategory(stream.readString());
        setPrice(stream.readDouble());
    }
    
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getProductID());
        stream.writeString(getName());
        stream.writeString(getCategory());
        stream.writeDouble(getPrice());
    }

    public void addToDatabase(Connection conn,String warehouseName,int quantity) throws ClassNotFoundException{
    try {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put("PRODUCT_TYPE", Class.forName("tables.Products"));
        String sql = "{call orders_utilities.addProduct(?,?,?)}";
        try(CallableStatement statement = conn.prepareCall(sql)){
                statement.setObject(1,this);
                statement.setString(2,warehouseName);
                statement.setInt(3,quantity);
                statement.execute();
        }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        
    }

    @Override
    public String toString() {
        return "productID= " + productID + ", name= " + name + ", category= " + category + ", price= " + price;
    }
    
}
