package tables;

import utilities.Validator;
public class Stores{

    public final String type_name = "STORES_TYPE";
    private int store_id;
    private String name;

    public Stores(int store_id, String name) {
        Validator check = new Validator();
        check.validateInt(store_id, 0, 99);
        check.validateString(name, 30);
        
        this.store_id = store_id;
        this.name = name;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Stores [store_id=" + store_id + ", name=" + name + "]";
    }
    
}
