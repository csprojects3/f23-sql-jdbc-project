package tables;

import java.sql.*;
import utilities.Validator;
import java.util.Map;


public class Orders implements SQLData{

    public final String type_name = "ORDERS_TYPE";
    private int order_id;
    private int customer_id;
    private int product_id;
    private int store_id;
    private int review_id;
    
    public Orders(int order_id, int customer_id, int product_id, int store_id, int review_id){
        Validator check = new Validator();
        check.validateInt(order_id, 0, 99);
        check.validateInt(customer_id, 0, 99);
        check.validateInt(product_id, 0, 99);
        check.validateInt(store_id, 0, 99);
        check.validateInt(review_id, 0, 99);

        this.order_id = order_id;
        this.customer_id = customer_id;
        this.product_id = product_id;
        this.store_id = store_id;
        this.review_id = review_id;
    }
    public Orders() {
    }
    public int getOrder_id() {
        return order_id;
    }
    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }
    public int getCustomer_id() {
        return customer_id;
    }
    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
    public int getProduct_id() {
        return product_id;
    }
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
    public int getStore_id() {
        return store_id;
    }
    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
    public int getReview_id() {
        return review_id;
    }
    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }
    public void addToDatabase(Connection conn,int quantity) throws ClassNotFoundException{
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("ORDER_TYPE", Class.forName("tables.Orders"));
            String sql = "{call orders_utilities.addOrder(?,?,?,?)}";
                try(CallableStatement statement = conn.prepareCall(sql)){
                        statement.setInt(1,quantity);
                        statement.setInt(2,this.customer_id);
                        statement.setInt(3,this.product_id);
                        statement.setInt(4,this.store_id);
                        statement.execute();
                    }
        }catch (SQLException e){
            e.printStackTrace();
        }     
    }
    
    //overrides
    @Override
    public String toString() {
        return "Orders [order_id=" + order_id + ", customer_id=" + customer_id + ", product_id=" + product_id
                + ", store_id=" + store_id + ", review_id=" + review_id + "]";
    }
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.type_name;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName ) throws SQLException {
        setOrder_id(stream.readInt());
        setCustomer_id(stream.readInt());
        setProduct_id(stream.readInt());
        setStore_id(stream.readInt());
        setReview_id(stream.readInt());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getOrder_id());
        stream.writeInt(getCustomer_id());
        stream.writeInt(getProduct_id());
        stream.writeInt(getStore_id());
        stream.writeInt(getReview_id());
    }
}
