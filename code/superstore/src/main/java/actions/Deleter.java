/**
 * a class with methods for deleting information from the database
 */

package actions;
import java.sql.*;

public class Deleter {
    public Deleter(){}

    /**
     * this method deletes an order based on the input order id
     * @param conn an sql connection object to connect to the database
     * @param orderId the id of an order in the database
     * @throws SQLException
     */
    public void deleteOrder(Connection conn, int orderId) throws SQLException{
        String sql = "{call orders_utilities.cancelOrder(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setInt(1,orderId);
        statement.execute();
    }

    /**
     * this method deletes a review based on the input review id
     * @param conn an sql connection object to connect to the database
     * @param reviewId the id of a review in the database
     * @throws SQLException
     */
    public void deleteReview(Connection conn, int reviewId) throws SQLException{
        String sql = "{call orders_utilities.removeReview(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setInt(1,reviewId);
        statement.execute();
    }

    /**
     * this method deletes a warehouse based on the input name
     * @param conn an sql connection object to connect to the database
     * @param warehouseName the name of a warehouse in the database
     * @throws SQLException
     */
    public void deleteWarehouse(Connection conn, String warehouseName) throws SQLException{
        String sql = "{call orders_utilities.deleteWarehouse(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setString(1,warehouseName);
        statement.execute();
    }
}
