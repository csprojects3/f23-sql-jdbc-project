git repo: https://gitlab.com/fall20233/fall23-databaseproject

Alec Morin	2239209
Adam Winter	2242433

how to setup database
1- run setup.sql
2- run inserts.sql
3- run types.sql
4- run package.sql
to drop tables run dropTable.sql

how to run java application
1- run app.java and type the number before each action to perform that action

Assumptions:
    tomato is 5$
    train X745 is 1 000 000$
    customers can only have 1 email registered
